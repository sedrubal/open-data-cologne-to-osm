"""Common tools to get data from offene-daten-koeln.de"""

import json
import os
import typing
from dataclasses import dataclass
from datetime import datetime, timedelta
from decimal import Decimal
from math import acos, cos, sin, tau
from pathlib import Path
from urllib.parse import urlencode

import httpx

from .logging import LOGGER

JSON_API_BASE_URL = "https://offenedaten-koeln.de/api/action/datastore/search.json"
DATA_STORAGE = Path("./.data/")
MAX_AGE = timedelta(days=7)

OSM_OVERPASS_API = "https://overpass-api.de/api/interpreter"

EARTH_RADIUS = Decimal(6371_009)

GENERATOR = "open data cologne - osm analyzer"


def check_if_cache_is_usable(file: Path) -> bool:
    is_usable = (
        file.is_file()
        and file.stat().st_size > 2
        and datetime.fromtimestamp(file.stat().st_mtime) > datetime.now() - MAX_AGE
    )

    if is_usable:
        LOGGER.debug(f"Using cache {file}")

    return is_usable


class OpenDataDownloader:
    DOWNLOAD_CHUNK_SIZE = 250

    def __init__(
        self,
        resource_id: str,
        cleanup_mapping: dict[
            str, typing.Callable[[str | int | float | None], typing.Any]
        ],
        filter_callback: typing.Callable[[dict[str, typing.Any]], bool],
    ):
        self.resource_id = resource_id
        self.cleanup_mapping = cleanup_mapping
        self.filter_callback = filter_callback
        self._total: int | None = None
        DATA_STORAGE.mkdir(parents=True, exist_ok=True)

    @property
    def _cache_file(self) -> Path:
        return DATA_STORAGE / f"{self.resource_id}.json"

    @property
    def total(self) -> int:
        if self._total is None:
            raise AssertionError(
                "You must start to download the data before we know how many entries there are."
            )

        return self._total

    def _clean_entry(
        self, entry: dict[str, str | int | float | None]
    ) -> dict[str, typing.Any]:
        for name, cleanup_func in self.cleanup_mapping.items():
            assert name in entry.keys()
            entry[name] = cleanup_func(entry[name])

        return entry

    @property
    async def entries(self) -> typing.AsyncIterable[typing.Any]:
        if check_if_cache_is_usable(self._cache_file):
            async for entry in self._load_cached_data():
                entry = self._clean_entry(entry)

                if self.filter_callback(entry):
                    yield entry
        else:
            async for entry in self._download_data():
                yield entry

    async def _load_cached_data(self) -> typing.AsyncIterator[typing.Any]:
        # TODO use proper json parser that supports streaming?
        with self._cache_file.open("r") as cache_file:
            assert cache_file.readline().strip() == "["

            while True:
                line = cache_file.readline()

                if line.strip() == "]":
                    return
                line = line.strip().rstrip(",")
                yield json.loads(line)

    async def _download_data(self) -> typing.AsyncIterator[typing.Any]:
        offset = 0

        async with httpx.AsyncClient() as client:
            with JSONStreamWriter(self._cache_file) as cache_file:
                while True:
                    response = await client.get(
                        JSON_API_BASE_URL,
                        params={
                            "resource_id": self.resource_id,
                            "limit": self.DOWNLOAD_CHUNK_SIZE,
                            "offset": offset,
                        },
                    )
                    response.raise_for_status()
                    data = response.json()
                    assert "success" in data.keys() and data["success"] is True
                    assert "result" in data.keys()
                    assert "resource_id" in data["result"].keys() and isinstance(
                        data["result"]["resource_id"], list
                    )
                    assert data["result"]["resource_id"][0] == self.resource_id
                    assert (
                        "limit" in data["result"].keys()
                        and data["result"]["limit"] == self.DOWNLOAD_CHUNK_SIZE
                    )
                    offset += int(data["result"]["limit"])
                    assert "total" in data["result"].keys() and (
                        self._total is None
                        or int(data["result"]["total"]) == self._total
                    )
                    self._total = int(data["result"]["total"])
                    assert "records" in data["result"]

                    for entry in data["result"]["records"]:
                        entry = self._clean_entry(entry)

                        if not self.filter_callback(entry):
                            continue
                        cache_file.append(entry)
                        yield entry

                    if offset >= self.total:
                        break


def get_osm_link(
    lon: Decimal | None = None,
    lat: Decimal | None = None,
    node_id: int | None = None,
    zoom: int = 19,
) -> str:
    query_dict: dict[str, typing.Any] = {
        "zoom": zoom,
    }

    if lon:
        query_dict["mlon"] = lon

    if lat:
        query_dict["mlat"] = lat
    query_string = urlencode(query_dict)
    base_url = f"https://osm.org/node/{node_id}" if node_id else "https://osm.org/"

    return f"{base_url}?{query_string}"


async def download_osm_data(
    lat1: float, lon1: float, lat2: float, lon2: float
) -> typing.AsyncIterable[typing.Any]:
    cache_path = (
        DATA_STORAGE / f"overpass-vending-machines-{lon1}-{lat1}-{lon2}-{lat2}.json"
    )
    node_id_lookup = set()

    if check_if_cache_is_usable(cache_path):
        with cache_path.open("r") as cache_file:
            for node in json.load(cache_file):
                if node["id"] in node_id_lookup:
                    assert False
                    breakpoint()
                node_id_lookup.add(node["id"])
                yield node

        return

    async with httpx.AsyncClient() as client:
        assert abs(lat2 - lat1) <= 1
        assert abs(lon2 - lon1) <= 1
        query = f"""
[out:json][timeout:25];
(
  node["amenity"="vending_machine"]["vending"="parking_tickets"]({lat1},{lon1},{lat2},{lon2});
);
out body;
>;
            """.strip()
        response = await client.post(
            OSM_OVERPASS_API,
            data={"data": query},
        )
        response.raise_for_status()
        data = response.json()
        assert "elements" in data.keys()

        with JSONStreamWriter(cache_path) as cache_file:
            for node in data["elements"]:
                assert node["id"] not in node_id_lookup
                node_id_lookup.add(node["id"])
                yield node
                cache_file.append(node)


class JSONStreamWriter:
    def __init__(self, path: Path):
        self.path: Path = path
        self._file: typing.BinaryIO | None = None
        self._data_added = False

    def __enter__(self):
        self._file = self.path.open("wb")
        self._file.write(b"[\n")
        return self

    def __exit__(self, *_args, **_kwargs):
        if self._file:
            if self._data_added:
                self._file.seek(-2, os.SEEK_CUR)
                self._file.write(b"\n]")
            self._file.close()

    def append(self, entry: dict[str, typing.Any]) -> None:
        assert self._file
        if isinstance(entry, dict):
            for name, value in entry.items():
                if isinstance(value, Decimal):
                    entry[name] = f"{value:f}"
                else:
                    entry[name] = value
        self._file.write(json.dumps(entry).encode())
        self._file.write(b",\n")
        self._data_added = True


class GeoIndex:
    @dataclass
    class Entry:
        lon: Decimal
        lat: Decimal
        node: typing.Any

    def __init__(
        self,
        get_lat: typing.Callable[[typing.Any], Decimal],
        get_lon: typing.Callable[[typing.Any], Decimal],
    ):
        self.get_lon: typing.Callable[[typing.Any], Decimal] = get_lon
        self.get_lat: typing.Callable[[typing.Any], Decimal] = get_lat
        self._by_lat: list[GeoIndex.Entry] = list[GeoIndex.Entry]()

    def __len__(self) -> int:
        return len(self._by_lat)

    def add(self, node: typing.Any):
        new_entry = GeoIndex.Entry(
            lon=self.get_lon(node),
            lat=self.get_lat(node),
            node=node,
        )
        for i, entry in enumerate(self._by_lat):
            if new_entry.lat <= entry.lat:
                self._by_lat.insert(i, new_entry)
                break
        else:
            self._by_lat.append(new_entry)

    def get_close_nodes(
        self, lon: Decimal, lat: Decimal, max_distance: Decimal | int = 20
    ) -> list[tuple[typing.Any, Decimal]]:
        def to_rad(deg: Decimal) -> Decimal:
            return deg / 360 * Decimal(f"{tau:.6f}")

        dtheta = max_distance / EARTH_RADIUS / Decimal(f"{tau:.6f}") * 360

        def binary_search(
            sorted_lst: list[GeoIndex.Entry],
            prop: typing.Literal["lon"] | typing.Literal["lat"],
            search_val: Decimal,
        ) -> list[GeoIndex.Entry]:
            results: list[GeoIndex.Entry] = list[GeoIndex.Entry]()
            search_range = [0, len(sorted_lst) - 1]
            min_val = search_val - dtheta
            max_val = search_val + dtheta

            while search_range[0] <= search_range[1]:
                mid_index = (search_range[0] + search_range[1]) // 2
                if min_val <= getattr(sorted_lst[mid_index], prop) <= max_val:
                    # hit
                    results.append(sorted_lst[mid_index])
                    # 1. go to left
                    idx = mid_index
                    while idx > 0:
                        if min_val > getattr(sorted_lst[idx - 1], prop):
                            break
                        idx -= 1
                        results.append(sorted_lst[idx])
                    # 2. go to right
                    idx = mid_index
                    while idx < len(sorted_lst) - 1:
                        if max_val < getattr(sorted_lst[idx + 1], prop):
                            break
                        idx += 1
                        results.append(sorted_lst[idx])

                    break
                elif getattr(sorted_lst[mid_index], prop) < min_val:
                    search_range[0] = mid_index + 1
                elif getattr(sorted_lst[mid_index], prop) > max_val:
                    search_range[1] = mid_index - 1

            return results

        # first search by lat
        results = binary_search(
            sorted_lst=self._by_lat,
            prop="lat",
            search_val=lat,
        )

        if not results:
            return []

        # sort results by lon
        results.sort(key=lambda entry: entry.lon)

        # now again with lon
        results = binary_search(
            sorted_lst=results,
            prop="lon",
            search_val=lon,
        )

        def get_exact_distance(
            lon1: Decimal,
            lat1: Decimal,
            lon2: Decimal,
            lat2: Decimal,
        ) -> Decimal:
            """d = r · arccos (sin φA sin φB + cos φA cos φB cos (λB - λA))"""
            lon1 = to_rad(lon1)
            lon2 = to_rad(lon2)
            lat1 = to_rad(lat1)
            lat2 = to_rad(lat2)

            distance = EARTH_RADIUS * Decimal(
                acos(sin(lon1) * sin(lon2) + cos(lon1) * cos(lon2) * cos(lat2 - lat1))
            )

            return distance

        results_with_distance: list[tuple[typing.Any, Decimal]] = [
            (
                entry.node,
                get_exact_distance(
                    lon1=entry.lon,
                    lat1=entry.lat,
                    lon2=lon,
                    lat2=lat,
                ),
            )
            for entry in results
        ]

        return [entry for entry in results_with_distance if entry[1] <= max_distance]


GPX_BASE_MARKUP = """
<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<gpx version="1.1" creator="{generator}" xmlns="http://www.topografix.com/GPX/1/1" xmlns:osmand="https://osmand.net">
  <metadata>
    <name>{name}</name>
    <time>{time}</time>
  </metadata>
  <extensions>
    <osmand:points_groups>
      <group name="{name}" color="#f00" icon="special_symbol_exclamation_mark" background="circle" />
    </osmand:points_groups>
  </extensions>
  {entries}
</gpx>
""".strip()

GPX_ENTRY_MARKUP = """
<wpt lat="{lat}" lon="{lon}">
  <time>{time}</time>
  <name>{title}</name>
  <desc>{desc}</desc>
  <type>{name}</type>
  <extensions>
    <osmand:icon>special_symbol_exclamation_mark</osmand:icon>
    <osmand:background>circle</osmand:background>
    <osmand:color>#f00</osmand:color>
  </extensions>
</wpt>
""".strip()


@dataclass
class TodoItem:
    lat: Decimal
    lon: Decimal
    title: str
    desc: str


def export_todo_gpx(export_path: Path, name: str, entries: list[TodoItem]) -> None:
    time = datetime.utcnow().isoformat(timespec="seconds")
    name = f"{name} TODOs from Open Data Cologne"
    xml_entries = list[str]()
    for entry in entries:
        xml_entries.append(
            GPX_ENTRY_MARKUP.format(
                lat=entry.lat,
                lon=entry.lon,
                time=time,
                title=entry.title,
                name=name,
                desc=entry.desc,
            )
        )
    markup = GPX_BASE_MARKUP.format(
        generator=GENERATOR,
        name=name,
        time=time,
        entries="\n".join(xml_entries),
    )

    with export_path.open("w") as export_file:
        export_file.write(markup)

    LOGGER.success(f"{export_path} written")


OSM_CHANGE_MARKUP = """
<osmChange version="0.6" generator="{generator}">
  <create>
    {creates}
  </create>
</osmChange>
""".strip()


def generate_osm_change(
    add_nodes: list[dict[str, str | int]], export_path: Path
) -> None:
    xml_nodes = list[str]()
    for counter, node in enumerate(add_nodes):
        lat = node.pop("lat")
        lon = node.pop("lon")
        xml_tags = "\n".join(
            f'<tag k="{key}" v="{value}"/>' for key, value in node.items()
        )
        xml_nodes.append(
            f"""
<node lat="{lat}" lon="{lon}" version="1" id="{-counter - 1}">
  {xml_tags}
</node>
            """.strip()
        )
    markup = OSM_CHANGE_MARKUP.format(
        generator=GENERATOR,
        creates="\n".join(xml_nodes),
    )

    with export_path.open("w") as export_file:
        export_file.write(markup)

    LOGGER.success(f"{export_path} written")
