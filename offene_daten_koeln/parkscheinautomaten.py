#!/usr/bin/env python3

import argparse
import asyncio
import json
import typing
from datetime import datetime
from decimal import Decimal
from pathlib import Path

from bs4 import BeautifulSoup

from .common import (
    GeoIndex,
    JSONStreamWriter,
    OpenDataDownloader,
    TodoItem,
    download_osm_data,
    export_todo_gpx,
    generate_osm_change,
    get_osm_link,
)
from .logging import LOGGER

RESSOURCE_ID = "11667407-a0d4-42ea-95d8-a0c5e40017dc"
IGNORE_PATH = Path("./.data/parkscheinautomaten-ignore.json")
OSM_CHANGE_EXPORT_PATH = Path("./parkscheinautomaten-change.osc")


def clean_lat(value: typing.Any) -> Decimal | None:
    if value:
        ret = Decimal(str(value).replace(",", "."))

        if 50 < ret < 52:
            return ret

        return None
    else:
        return None


def clean_lon(value: typing.Any) -> Decimal | None:
    if value:
        ret = Decimal(str(value).replace(",", "."))

        if 6 < ret < 8:
            return ret

        return None
    else:
        return None


def clean_str(value: typing.Any) -> str:
    return str(value).strip()


def clean_str_or_none(value: typing.Any) -> str | None:
    return clean_str(value) if value else None


OPEN_DATA_CLEANUP_MAPPING: dict[
    str, typing.Callable[[str | int | float | None], typing.Any]
] = {
    "psa_nr_": int,
    "aufstellort": clean_str,
    "bezirkgebiet": clean_str,
    "abschnitt_von": clean_str,
    "abschnitt_bis": clean_str,
    "stellpltze": int,
    "roter_punkt": clean_str_or_none,
    "gebuehrenzeit": clean_str,
    "gebuehr_je_15_minuten": lambda value: Decimal(
        str(value).replace(",", ".").replace("€", "").strip()
    ),
    "hpdstd_": int,
    "tagesgebuehr": clean_str_or_none,
    "tarifzone": clean_str,
    "geokoordinatenord": clean_lat,
    "geokoordinateost": clean_lon,
}


def open_data_filter(entry: dict[str, typing.Any]) -> bool:
    return entry["geokoordinateost"] and entry["geokoordinatenord"]


async def _run() -> None:
    with IGNORE_PATH.open("r") as ignore_file:
        ignored_psas = set(json.load(ignore_file))

    opendata = OpenDataDownloader(
        resource_id=RESSOURCE_ID,
        cleanup_mapping=OPEN_DATA_CLEANUP_MAPPING,
        filter_callback=open_data_filter,
    )

    min_lat = min_lon = float("inf")
    max_lat = max_lon = -float("inf")

    open_data_entries = list[dict]()
    async for entry in opendata.entries:
        if entry["psa_nr_"] in ignored_psas:
            LOGGER.warning(f"Ignoring {entry['psa_nr_']}")

            continue

        if entry["geokoordinateost"]:
            min_lat = min(min_lat, entry["geokoordinatenord"])
            max_lat = max(max_lat, entry["geokoordinatenord"])

        if entry["geokoordinatenord"]:
            min_lon = min(min_lon, entry["geokoordinateost"])
            max_lon = max(max_lon, entry["geokoordinateost"])

        open_data_entries.append(entry)

    LOGGER.info(f"loaded {len(open_data_entries)} entries from open data")
    LOGGER.debug(f"boundaries: {min_lon}:{min_lat} - {max_lon}:{max_lat}")

    osm_idx = GeoIndex(
        get_lat=lambda node: Decimal(f"{node['lat']:.6f}"),
        get_lon=lambda node: Decimal(f"{node['lon']:.6f}"),
    )
    async for node in download_osm_data(
        lat1=min_lat,
        lon1=min_lon,
        lat2=max_lat,
        lon2=max_lon,
    ):
        osm_idx.add(node)

    LOGGER.info(f"loaded {len(osm_idx)} nodes from OSM")
    max_dist = 20
    todos = list[TodoItem]()

    for entry in open_data_entries:
        lat = entry["geokoordinatenord"]
        lon = entry["geokoordinateost"]
        close_osm_nodes = osm_idx.get_close_nodes(
            lat=lat,
            lon=lon,
            max_distance=max_dist,
        )

        if not close_osm_nodes:
            psa_nr = entry["psa_nr_"]
            LOGGER.error(
                f"PSA#{psa_nr}: Found no nearby osm features within {max_dist}m: {get_osm_link(lat=lat, lon=lon)}"
            )
            todos.append(
                TodoItem(
                    lat=lat,
                    lon=lon,
                    title=f"Parkscheinautomat #{psa_nr}",
                    desc=f"""
According to Open Data Cologne there must be a parking ticket vending machine.

Ort: {entry['aufstellort']}
Bezirksgebiet: {entry['bezirkgebiet']}
Abschnitt: {entry['abschnitt_von']} - {entry['abschnitt_bis']}
Stellplätze: {entry['stellpltze']}
Roter Punkt: {entry["roter_punkt"] or 'nein'}
Gebührenzeit: {entry["gebuehrenzeit"]}
Gebühr je 15 min: {f'{entry["gebuehr_je_15_minuten"]} €' if entry["gebuehr_je_15_minuten"] else 'nein'}
Tagesgebühr: {f'{entry["tagesgebuehr"]} €' if entry["tagesgebuehr"] else 'nein'}
Tarifzone: {entry["tarifzone"]}
hpdstd: {entry["hpdstd_"]}
""".strip(),
                )
            )
        elif len(close_osm_nodes) == 1:
            osm_node_id = close_osm_nodes[0][0]["id"]
            dist = close_osm_nodes[0][1]
            LOGGER.success(
                f"PSA#{entry['psa_nr_']}: Found 1 nearby osm features: within {dist:.0f}m {get_osm_link(node_id=osm_node_id, lat=lat, lon=lon)}"
            )
        else:
            LOGGER.warning(
                f"PSA#{entry['psa_nr_']}: Found {len(close_osm_nodes)} nearby osm features"
            )

            for osm_node, distance in close_osm_nodes:
                osm_node_id = osm_node["id"]
                LOGGER.info(
                    f"  within {distance:.0f}m {get_osm_link(node_id=osm_node_id, lat=lat, lon=lon)}"
                )

    export_todo_gpx(
        export_path=Path("./parkscheinautomaten-todos.gpx"),
        name="Parkscheinautomaten",
        entries=todos,
    )


def after_survey():
    after_survey_gpx_path = Path("./parkscheinautomaten-local-survey.gpx")
    with after_survey_gpx_path.open("r") as after_survey_gpx_file:
        after_survey_gpx = BeautifulSoup(after_survey_gpx_file.read(), features="xml")

    osm_add_nodes = list[dict[str, str | int]]()
    with JSONStreamWriter(IGNORE_PATH) as ignore_file:
        for entry in after_survey_gpx.gpx.find_all("wpt"):
            lat = Decimal(entry.attrs["lat"])
            lon = Decimal(entry.attrs["lon"])
            psa_nr = int(
                entry.find("name").get_text().replace("Parkscheinautomat #", "").strip()
            )
            color = entry.find("osmand:color").get_text()
            time = datetime.fromisoformat(entry.find("time").get_text())
            if color == "#ff0000":
                # not yet edited
                continue
            elif color == "#000001":
                # ignore this psa
                LOGGER.warning(
                    f"PSA#{psa_nr} -> ignore ({get_osm_link(lat=lat, lon=lon)})"
                )
                ignore_file.append(psa_nr)
            elif color == "#00842b":
                # accept
                LOGGER.success(
                    f"PSA#{psa_nr} -> will be added ({get_osm_link(lat=lat, lon=lon)})"
                )
                osm_add_nodes.append(
                    {
                        "lat": lat,
                        "lon": lon,
                        "amenity": "vending_machine",
                        "vending": "parking_tickets",
                        "source:date": time.date().isoformat(),
                        "operator": "Stadt Köln",
                        "ref": psa_nr,
                        "source": "survey;https://offenedaten.koeln/",
                        "payment:coins": "yes",
                        "payment:ep_geldkarte": "yes",
                        "currency:EUR": "yes",
                    }
                )
            else:
                assert False

    if osm_add_nodes:
        generate_osm_change(add_nodes=osm_add_nodes, export_path=OSM_CHANGE_EXPORT_PATH)


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("action", choices={"find-missing", "after-survey"})
    args = parser.parse_args()
    if args.action == "find-missing":
        asyncio.run(_run())
    elif args.action == "after-survey":
        after_survey()
    else:
        assert False


if __name__ == "__main__":
    main()
